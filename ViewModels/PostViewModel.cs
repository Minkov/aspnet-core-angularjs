using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Forum.Models;

namespace Forum.ViewModels
{
    public class PostViewModel
    {
        public static Expression<Func<Post, PostViewModel>> FromModel
        {
            get
            {
                return model => new PostViewModel
                {
                    Id = model.Id,
                    Title = model.Title,
                    TotalRating = model.Ratings.Count > 0
                        ? model.Ratings.Select(rating => rating.Value)
                            .Average()
                        : 0,
                    Tags = model.PostTags.Select(postTag => postTag.Tag)
                        .AsQueryable()
                        .Select(SimpleTagViewModel.FromModel)
                        .ToList(),

                    Category = SimpleCategoryViewModel.FromModel(model.Category),
                    CreatedOn = model.CreatedOn
                };
            }
        }

        public int Id { get; private set; }
        public string Title { get; private set; }
        public double TotalRating { get; private set; }
        public IEnumerable<SimpleTagViewModel> Tags { get; private set; }
        public SimpleCategoryViewModel Category { get; private set; }
        public DateTime? CreatedOn { get; private set; }
    }
}