using System.Collections.Generic;

namespace Forum.ViewModels
{
    public class CreatePostViewModel
    {
        public string Title { get; set; }

        public string Content { get; set; }

        public string CategoryName { get; set; }

        public IEnumerable<string> Tags { get; set; }
    }
}