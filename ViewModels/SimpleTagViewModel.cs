using System;
using System.Linq.Expressions;
using Forum.Models;

namespace Forum.ViewModels
{
    public class SimpleTagViewModel
    {
        public static Expression<Func<Tag, SimpleTagViewModel>> FromModel
        {
            get
            {
                return model => new SimpleTagViewModel
                {
                    Id = model.Id,
                    Name = model.Name
                };
            }
        }
        public int Id { get; private set; }
        public string Name { get; private set; }
    }
}