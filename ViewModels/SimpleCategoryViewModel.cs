using System;
using Forum.Models;

namespace Forum.ViewModels
{
    public class SimpleCategoryViewModel
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public DateTime? CreatedOn { get; private set; }

        public static SimpleCategoryViewModel FromModel(Category category)
        {
            return new SimpleCategoryViewModel
            {
                Id = category.Id,
                Name = category.Name,
                CreatedOn = category.CreatedOn,
            };
        }
    }
}