namespace Forum.Data.Contracts {
    public interface ISaver
    {
        void Save();
    }
}