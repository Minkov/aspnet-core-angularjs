using System.Linq;
using Forum.Models.Abstracts;

namespace Forum.Data.Contracts
{
    public interface IRepository<T>
    where T : class, IDeletable
    {
        IQueryable<T> All { get; }
        IQueryable<T> AllAndDeleted { get; }

        T GetById(int id);

        void Add(T entity);
        void Delete(T entity);
        void Update(T entity);
    }
}