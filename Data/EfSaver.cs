using Forum.Data.Contracts;

namespace Forum.Data
{
    public class EfSaver : ISaver
    {
        private readonly ForumDbContext dbContext;

        public EfSaver(ForumDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void Save()
        {
            this.dbContext.SaveChanges();
        }
    }
}