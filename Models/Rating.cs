using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Forum.Models.Abstracts;

namespace Forum.Models
{
    public class Rating : DataModel
    {
        public int Value { get; set; }
    }
}