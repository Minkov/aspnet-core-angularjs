using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Forum.Models.Abstracts;

namespace Forum.Models
{
    public class Category : DataModel
    {
        public Category()
        {
            this.Posts = new HashSet<Post>();
        }

        [Required]
        public string Name { get; set; }

        public ICollection<Post> Posts { get; set; }
    }
}