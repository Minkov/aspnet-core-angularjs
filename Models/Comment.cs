using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Forum.Models.Abstracts;

namespace Forum.Models
{
    public class Comment : DataModel
    {
        public Comment()
        {
            this.Rating = new HashSet<Rating>();
        }

        [Required]
        public string Text { get; set; }

        public Comment ParentComment { get; set; }

        public ICollection<Rating> Rating { get; set; }
    }
}