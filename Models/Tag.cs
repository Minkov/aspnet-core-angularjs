using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Forum.Models.Abstracts;

namespace Forum.Models
{
    public class Tag : DataModel
    {

        public Tag()
        {
            this.TagPosts = new HashSet<PostTag>();
        }
        public string Name { get; set; }

        public ICollection<PostTag> TagPosts { get; set; }
    }

}