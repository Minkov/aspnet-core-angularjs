using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Forum.Models.Abstracts;

namespace Forum.Models
{
    public class Post : DataModel
    {
        public Post()
        {
            this.PostTags = new HashSet<PostTag>();
            this.Ratings = new HashSet<Rating>();
            this.Comments = new HashSet<Comment>();
        }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Content { get; set; }

        [Required]
        public int CategoryId { get; set; }
        
        public virtual Category Category { get; set; }

        public virtual ICollection<PostTag> PostTags { get; set; }

        public virtual ICollection<Rating> Ratings { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
    }
}