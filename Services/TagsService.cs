using System.Collections.Generic;
using System.Linq;
using Forum.Data.Contracts;
using Forum.Models;
using Forum.Services.Contracts;

namespace Forum.Services
{
    public class TagsService : ITagsService
    {
        private readonly IRepository<Tag> tagsRepository;

        public TagsService(IRepository<Tag> tagsRepository)
        {
            this.tagsRepository = tagsRepository;
        }
        public Tag CreateTag(string name)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Tag> GetAll()
        {
            return this.tagsRepository.All.ToList();
        }

        public Tag GetTagByName(string name)
        {
            throw new System.NotImplementedException();
        }
    }
}