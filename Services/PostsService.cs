using System;
using System.Collections.Generic;
using System.Linq;
using Forum.Data.Contracts;
using Forum.Models;
using Forum.Services.Contracts;
using Microsoft.EntityFrameworkCore;

namespace Forum.Services
{
    public class PostsService : IPostsService
    {
        private readonly IRepository<Post> postsRepository;
        private readonly IRepository<Category> categoriesRepository;
        private readonly IRepository<Tag> tagsRepository;
        private readonly ISaver saver;

        public PostsService(
            IRepository<Post> postsRepository,
            IRepository<Category> categoriesRepository,
            IRepository<Tag> tagsRepository,
            ISaver saver)
        {
            this.postsRepository = postsRepository;
            this.categoriesRepository = categoriesRepository;
            this.tagsRepository = tagsRepository;
            this.saver = saver;
        }

        public Post CreatePost(string title, string content, string categoryName, IEnumerable<string> tagNames)
        {
            var category = this.LoadOrCreateCategory(categoryName);
            var tags = tagNames.Select(tagName => this.LoadOrCreateTag(tagName));
            var model = new Post
            {
                Title = title,
                Category = category,
                Content = content,
            };

            model.PostTags = tags.Select(tag => new PostTag
            {
                Tag = tag,
                Post = model,
            })
            .ToList();

            this.postsRepository.Add(model);


            saver.Save();
            return model;
        }

        public IEnumerable<Post> GetAllPosts()
        {
            return this.postsRepository.All.Include(post => post.Category)
                .Include(post => post.PostTags)
                .ThenInclude(postTag => postTag.Tag)
                .Include(post => post.Ratings)
                .ToList();
        }

        public IEnumerable<Post> GetPostsByCategory(string categoryName)
        {
            var category = this.categoriesRepository.All
                .Where(cat => cat.Name.ToLower() == categoryName.ToLower())
                .FirstOrDefault();
            return this.postsRepository.All
                    .Where(post => post.CategoryId == category.Id)
                    .Include(post => post.Category)
                   .Include(post => post.PostTags)
                   .ThenInclude(postTag => postTag.Tag)
                   .Include(post => post.Ratings)
                   .ToList();
        }

        public IEnumerable<Post> SearchPostsByTitle(string titlePattern)
        {
            return this.postsRepository.All.Include(post => post.Category)
                .Include(post => post.PostTags)
                .Include(post => post.Ratings)
                .Where(
                    post =>
                        post.Title.ToLower()
                            .Contains(titlePattern.ToLower()));
        }

        private Category LoadOrCreateCategory(string categoryName)
        {
            var category = this.categoriesRepository.All.Where(
                cat => cat.Name.ToLower() == categoryName)
                .FirstOrDefault();

            if (category == null)
            {
                category = new Category
                {
                    Name = categoryName
                };

                this.categoriesRepository.Add(category);
            }
            return category;
        }

        private Tag LoadOrCreateTag(string tagName)
        {
            var tag = this.tagsRepository.All.Where(
                  cat => cat.Name.ToLower() == tagName)
                  .FirstOrDefault();

            if (tag == null)
            {
                tag = new Tag
                {
                    Name = tagName
                };

                this.tagsRepository.Add(tag);
            }

            return tag;
        }
    }
}