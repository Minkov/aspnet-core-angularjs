using System.Collections.Generic;
using Forum.Models;

namespace Forum.Services.Contracts
{
    public interface ICategoriesService
    {
        Category GetCategoryByName(string name);

        Category CreateCategory(string name);

        IEnumerable<Category> GetAll();
     }
}