using System.Collections.Generic;
using Forum.Models;

namespace Forum.Services.Contracts
{
    public interface IPostsService
    {
        IEnumerable<Post> GetAllPosts();

        IEnumerable<Post> SearchPostsByTitle(string titlePattern);

        IEnumerable<Post> GetPostsByCategory(string categoryId);

        Post CreatePost(string title, string content, string categoryName, IEnumerable<string> tags);
    }
}