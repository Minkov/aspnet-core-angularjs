using System.Collections.Generic;
using Forum.Models;

namespace Forum.Services.Contracts
{
    public interface ITagsService
    {
        Tag GetTagByName(string name);

        Tag CreateTag(string name);

        IEnumerable<Tag> GetAll();
    }
}