using System.Collections.Generic;
using System.Linq;
using Forum.Data.Contracts;
using Forum.Models;
using Forum.Services.Contracts;

namespace Forum.Services
{
    public class CategoriesService : ICategoriesService
    {
        private readonly IRepository<Category> categoriesRepository;

        public CategoriesService(IRepository<Category> categoriesRepository)
        {
            this.categoriesRepository = categoriesRepository;
        }
        public Category CreateCategory(string name)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Category> GetAll()
        {
            return categoriesRepository.All.ToList();
        }

        public Category GetCategoryByName(string name)
        {
            throw new System.NotImplementedException();
        }
    }
}