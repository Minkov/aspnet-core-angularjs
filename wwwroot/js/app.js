angular.module('forumapp', ['ngRoute'])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                controller: 'PostsController',
                templateUrl: 'views/pages/posts.html'
            })
            .when('/posts', {
                controller: 'PostsController',
                templateUrl: 'views/pages/posts.html'
            })
            .when('/posts/create', {
                controller: 'PostCreateController',
                templateUrl: 'views/pages/create-post.html',
            });
    })
    .factory('categoriesData', function ($http) {
        var getAll = function () {
            return $http.get('/api/categories')
                .then(function (response) {
                    return response.data;
                });
        };

        return {
            getAll: getAll,
        };
    })
    .factory('tagsData', function ($http) {
        var getAll = function () {
            return $http.get('/api/tags')
                .then(function (response) {
                    return response.data;
                });
        };

        return {
            getAll,
        };
    })
    .factory('postsData', function ($http) {
        var getAll = function () {
            return $http.get('/api/posts')
                .then(function (response) {
                    return response.data;
                });
        };

        var getByCategory = function (category) {
            return $http.get('/api/posts?category=' + category)
                .then(function (response) {
                    return response.data;
                });
        };


        var create = function (post) {
            return $http.post('/api/posts', post)
                .then(function (response) {
                    return response.data;
                });
        };
        return {
            getAll: getAll,
            getByCategory: getByCategory,
            create: create,
        };
    })
    .component('postItem', {
        controller: 'PostItemController',
        templateUrl: 'views/components/post-item.html',
        bindings: {
            post: '='
        }
    })
    .controller('PostsController', function ($scope, $routeParams, postsData) {
        console.log($routeParams);
        $scope.title = 'Welcome to our forum!';
        var promise;
        if ($routeParams.category) {
            $scope.selectedCategory = $routeParams.category;
            promise = postsData.getByCategory($scope.selectedCategory);
        } else {
            promise = postsData.getAll();
        }

        promise.then(function (posts) {
            $scope.posts = posts;
        });
    })
    .controller('PostCreateController', function ($scope, postsData, tagsData, categoriesData) {
        $scope.post = {
            title: '',
            content: '',
            category: '',
            tags: [],
        };

        categoriesData.getAll()
            .then((categories) => {
                console.log(categories);
                $scope.categories = categories;
            });

        tagsData.getAll()
            .then((tags) => {
                $scope.tags = tags;
            });

        $scope.updateTag = function (tag) {
            var tagInPostIndex = $scope.post.tags.findIndex(t => t.id === tag.id);
            if (tagInPostIndex < 0) {
                $scope.post.tags.push(tag);
            } else {
                $scope.post.tags.splice(tagInPostIndex, 1);
            }
        };

        $scope.save = function () {
            postsData.create($scope.post)
                .then(() => {
                    $scope.post = {
                        title: '',
                        content: '',
                        category: '',
                        tags: [],
                    };
                });
        };
    })
    .controller('PostItemController', function ($scope) {
        var vm = this;
        console.log(vm);
        console.log($scope);
    });