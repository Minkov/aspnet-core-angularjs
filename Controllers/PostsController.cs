using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Forum.Models;
using Forum.Services.Contracts;
using Forum.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Forum.Controllers
{
    public class PostsController : Controller
    {
        private readonly IPostsService postsService;

        public PostsController(IPostsService postsService)
        {
            this.postsService = postsService;
        }

        // [HttpGet]
        // [Route("/api/posts")]
        // public ActionResult GetAll()
        // {
        //     var models = this.postsService.GetAllPosts();

        //     var viewModels = models.AsQueryable()
        //         .Select(PostViewModel.FromModel)
        //         .ToList();

        //     return Json(viewModels);
        // }

        [HttpGet]
        [Route("/api/posts")]
        public ActionResult GetByCategory([FromQuery(Name="category")] string categoryName = "")
        {

            var models = categoryName == string.Empty
                ? this.postsService.GetAllPosts()
                : this.postsService.GetPostsByCategory(categoryName);

            var viewModels = models.AsQueryable()
                .Select(PostViewModel.FromModel)
                .ToList();

            return Json(viewModels);
        }

        [Route("/api/posts")]
        [HttpPost]
        public async Task<ActionResult> Create([FromBody] CreatePostViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var model = this.postsService.CreatePost(
                    viewModel.Title,
                    viewModel.Content,
                    viewModel.CategoryName,
                    viewModel.Tags);

                var viewModelToReturn = Enumerable.Repeat(model, 1)
                    .AsQueryable()
                    .Select(PostViewModel.FromModel)
                    .FirstOrDefault();

                return Json(viewModelToReturn);
            }

            return null;
        }
    }
}