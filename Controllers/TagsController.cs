using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Forum.Models;
using Forum.Services.Contracts;
using Forum.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Forum.Controllers
{
    public class TagsController : Controller
    {
        private readonly ITagsService tagsService;

        public TagsController(ITagsService tagsService)
        {
            this.tagsService = tagsService;
        }

        [HttpGet]
        [Route("/api/tags")]
        public ActionResult GetAll()
        {
            var tags = this.tagsService.GetAll()
                .AsQueryable()
                .Select(SimpleTagViewModel.FromModel);

            return Json(tags);
        }
    }
}