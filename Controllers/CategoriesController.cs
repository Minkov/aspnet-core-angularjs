using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Forum.Models;
using Forum.Services.Contracts;
using Forum.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Forum.Controllers
{
    public class CategoriesController : Controller
    {
        private readonly ICategoriesService categoriesService;

        public CategoriesController(ICategoriesService categoriesService)
        {
            this.categoriesService = categoriesService;
        }

        [HttpGet]
        [Route("/api/categories")]
        public ActionResult GetAll()
        {
            var categories = this.categoriesService.GetAll()
                .AsQueryable()
                .Select(SimpleCategoryViewModel.FromModel);

            return Json(categories);
        }
    }
}